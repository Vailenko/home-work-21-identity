﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;

namespace ConsoleApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Please type 'y' to confirm Identity service calling or other key for exit: ");
                if (Console.ReadLine().ToLower() != "y") break;


                var client = new HttpClient();

                var discovery = await client.GetDiscoveryDocumentAsync("https://localhost:6001");

                if (discovery.IsError)
                {
                    Console.WriteLine($"Document discovery error:{discovery.Error}");
                    continue;
                }

                var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
                {
                    Address = discovery.TokenEndpoint,
                    ClientId = "vail.client",
                    ClientSecret = "very-strong-secret",
                    Scope = "scopeAmerica"
                });

                if (tokenResponse.IsError)
                {
                    Console.WriteLine($"Token response error:{tokenResponse.Error}");
                    continue;
                }

                client.SetBearerToken(tokenResponse.AccessToken);



                var response = await client.GetAsync("https://localhost:5001/identity");
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Console.WriteLine($"Token {tokenResponse.TokenType} response: {tokenResponse.AccessToken} ");
                    Console.WriteLine($"Identity controller response: {content}");
                }
                else
                {
                    Console.WriteLine($"Identinty controlles return error code: {response.StatusCode}");
                }

            }
        }
    }
}

